import { Test, TestingModule } from '@nestjs/testing';
import { CoachingMentoringService } from './coaching-mentoring.service';

describe('CoachingMentoringService', () => {
  let service: CoachingMentoringService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CoachingMentoringService],
    }).compile();

    service = module.get<CoachingMentoringService>(CoachingMentoringService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
