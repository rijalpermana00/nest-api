import { ApiProperty } from "@nestjs/swagger";
import { Transform } from "class-transformer";
import { IsDate, IsDateString, IsInt, IsNotEmpty, IsString } from "class-validator";

export class CreateCoachingMentoringDto {
    @ApiProperty()
    @IsNotEmpty()
    @IsInt()  
    groupId: number;
    
    @ApiProperty()
    @IsNotEmpty()
    @IsInt()  
    activityId: number;
    
    @ApiProperty()
    @IsNotEmpty()
    @IsString()  
    agenda: string;
    
    @ApiProperty()
    @IsNotEmpty()
    @IsInt()
    leaderId: number;
    
    @ApiProperty()
    @IsNotEmpty()
    @IsInt()  
    employeeId: number;
    
    // @ApiProperty()
    // @IsNotEmpty()
    // @IsString()  
    // result: string;
    
    // @ApiProperty()
    // @IsNotEmpty({
    //     message: 'plan should be inputed'
    // })
    // @IsString({
    //     message: 'plan should be string'
    // })
    // plan: string;
    
    @ApiProperty()
    @Transform(({ value }) => new Date(value))
    appointmentTime: Date;
    
    // @ApiProperty()
    // @Transform(({ value }) => new Date(value))
    // approvedAt: Date;
    
    // @ApiProperty() 
    // @Transform(({ value }) => new Date(value))
    // rejectedAt: Date;
}

