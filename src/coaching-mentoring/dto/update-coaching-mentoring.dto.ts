import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsInt, IsNotEmpty, IsString } from 'class-validator';
import { CreateCoachingMentoringDto } from './create-coaching-mentoring.dto';

export class UpdateCoachingMentoringDto extends PartialType(CreateCoachingMentoringDto) {
    @ApiProperty()
    @IsNotEmpty()
    @IsString()  
    agenda: string;
    
    @ApiProperty()
    @IsNotEmpty()
    @IsString()  
    result: string;
    
    @ApiProperty()
    @IsNotEmpty({
        message: 'plan should be inputed'
    })
    @IsString({
        message: 'plan should be string'
    })
    plan: string;
}
