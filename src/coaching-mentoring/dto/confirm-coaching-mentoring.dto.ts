import { ApiProperty } from "@nestjs/swagger";
import { Transform } from "class-transformer";
import { IsDate, IsDateString, IsInt, IsNotEmpty, IsOptional, IsString } from "class-validator";

export class ConfirmCoachingMentoringDto {
    @ApiProperty()
    @IsNotEmpty({
        message: "appointment time cannot be empty"
    })
    @Transform(({ value }) => new Date(value))
    appointmentTime: Date;
    
    @ApiProperty()
    @IsOptional()
    agenda: string;
}

