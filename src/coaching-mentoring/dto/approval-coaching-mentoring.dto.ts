import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsEnum, IsInt, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { CreateCoachingMentoringDto } from './create-coaching-mentoring.dto';

export class ApprovalCoachingMentoringDto extends PartialType(CreateCoachingMentoringDto) {
    @ApiProperty()
    @IsNotEmpty()
    @IsEnum({
        APPROVED: 'APPROVED',
        REJECTED: 'REJECTED'
    })
    status: string;
    
    @ApiProperty()
    @IsOptional()
    plan: string;
    
    @ApiProperty()
    @IsOptional()  
    result: string;
    
}
