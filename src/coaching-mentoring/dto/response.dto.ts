import { CoachingMentoring } from "../entities/coaching-mentoring.entity";

export interface Response{
    status: boolean;
    message: string;
    data: CoachingMentoring;
}