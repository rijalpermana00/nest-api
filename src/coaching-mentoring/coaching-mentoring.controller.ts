import { Controller, Get, Post, Body, Patch, Param, Delete, HttpException, HttpStatus, NotFoundException, Put } from '@nestjs/common';
import { CoachingMentoringService } from './coaching-mentoring.service';
import { CreateCoachingMentoringDto } from './dto/create-coaching-mentoring.dto';
import { UpdateCoachingMentoringDto } from './dto/update-coaching-mentoring.dto';
import { ConfirmCoachingMentoringDto } from './dto/confirm-coaching-mentoring.dto';
import { ApprovalCoachingMentoringDto } from './dto/approval-coaching-mentoring.dto';

@Controller('coaching')
export class CoachingMentoringController {
    constructor(private readonly coachingMentoringService: CoachingMentoringService) {}

    @Post()
    async create(@Body() createCoachingMentoringDto: CreateCoachingMentoringDto) {
        
        try{
            
            const coachingMentoring = await this.coachingMentoringService.create(createCoachingMentoringDto);
            
            return coachingMentoring;
            
        }catch(e){
            
            throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
        }
    }
    
    @Patch('confirm/:id')
    async confirm(
        @Param('id') id: string,
        @Body() confirmCoachingMentoringDto: ConfirmCoachingMentoringDto
    ){
        try{
            
            const coaching = await this.coachingMentoringService.confirm(id,confirmCoachingMentoringDto);
            
            return coaching;
            
        }catch(e){
            
            throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
        }
    }

    @Get()
    findAll() {
        return this.coachingMentoringService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.coachingMentoringService.findOne(id);
    }

    @Patch('update/:id')
    async update(
        @Param('id') id: string, 
        @Body() updateCoachingMentoringDto: UpdateCoachingMentoringDto
    ){
        try{
            
            const coaching = await this.coachingMentoringService.update(id,updateCoachingMentoringDto);
            
            return coaching;
            
        }catch(e){
            
            throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
        }
    }
    
    @Put('approval/:id')
    async approval(
        @Param('id') id: string,
        @Body() approvalDto: ApprovalCoachingMentoringDto
    ){
        try{
            
            const coaching = await this.coachingMentoringService.approval(id,approvalDto);
            
            return coaching;
            
        }catch(e){
            
            throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
        }
    }
}
