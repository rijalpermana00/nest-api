import { HttpException, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PostgresError } from 'pg-error-enum';
import { Repository } from 'typeorm';
import { ApprovalCoachingMentoringDto } from './dto/approval-coaching-mentoring.dto';
import { ConfirmCoachingMentoringDto } from './dto/confirm-coaching-mentoring.dto';
import { CreateCoachingMentoringDto } from './dto/create-coaching-mentoring.dto';
import { Response } from './dto/response.dto';
import { UpdateCoachingMentoringDto } from './dto/update-coaching-mentoring.dto';
import { CoachingMentoring } from './entities/coaching-mentoring.entity';

@Injectable()
export class CoachingMentoringService {
  constructor(
    @InjectRepository(CoachingMentoring)
    private coachingMentoringRepository: Repository<CoachingMentoring>,
  ) { }
  
  async create(createCoachingMentoringDto: CreateCoachingMentoringDto): Promise<CoachingMentoring> {
    const coachingMentoring = new CoachingMentoring({
      group_id : createCoachingMentoringDto.groupId,
      activity_id : createCoachingMentoringDto.activityId,
      agenda : createCoachingMentoringDto.agenda,
      leader_id : createCoachingMentoringDto.leaderId,
      employee_id : createCoachingMentoringDto.employeeId,
      // result : createCoachingMentoringDto.result,
      // plan : createCoachingMentoringDto.plan,
      appointment_time : createCoachingMentoringDto.appointmentTime,
      // approved_at : createCoachingMentoringDto.approvedAt,
      // rejected_at : createCoachingMentoringDto.rejectedAt,
    });

    return await this.coachingMentoringRepository.save(coachingMentoring);
  }

  async findAll() {
    return await this.coachingMentoringRepository.find();
  }

  async findOne(id: string): Promise<CoachingMentoring>{
    const one = await this.coachingMentoringRepository.findOne({
      where: {id:id},
    });
    
    return one;
  }

  async confirm(id: string,confirmCoachingMentoringDto: ConfirmCoachingMentoringDto): Promise<Response> {
    
    try{
      
      const one = await this.findOne(id);
      console.log(one);
      if(!one){
        throw new NotFoundException('Data not found');
      }
      
      one.agenda = confirmCoachingMentoringDto.agenda;
      one.appointment_time =confirmCoachingMentoringDto.appointmentTime;
      one.status = 1;
      
      const test = await this.coachingMentoringRepository.save(one);
      console.log(test)
      return {
        status: true,
        message: 'success',
        data: test,
      }
      
    }catch(e){
      return {
        status: false,
        message: e.message,
        data: null,
      }
      // throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
    }
    
  }

  async update(id: string, updateCoachingMentoringDto: UpdateCoachingMentoringDto) {
    try{
      
      const one = await this.findOne(id);
      
      if(!one){
        return {
          status: false,
          message: 'data not found',
          data: null,
        }
      }
      
      if(one.status === 3){
        return {
          status: false,
          message: 'data is already approved',
          data: null,
        }
      }
      
      one.agenda = updateCoachingMentoringDto.agenda;
      one.result =updateCoachingMentoringDto.result;
      one.plan =updateCoachingMentoringDto.plan;
      one.status = 2;
      
      await this.coachingMentoringRepository.save(one);
      
      return {
        status: true,
        message: 'success',
        data: one,
      }
      
    }catch(e){
      return {
        status: false,
        message: e.message,
        data: null,
      }
      // throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
    }
  }
  
  async approval(id: string, approvalDto: ApprovalCoachingMentoringDto) {
    try{
      
      const one = await this.findOne(id);
      
      if(!one){
        return {
          status: false,
          message: 'data not found',
          data: null,
        }
      }
      
      if(one.status === 3){
        return {
          status: false,
          message: 'data is already approved',
          data: null,
        }
      }
      
      if(approvalDto.status === 'APPROVED'){
        one.status = 3;
        one.approved_at = new Date();
      
      }else if(approvalDto.status === 'REJECTED'){
        one.status = 4;
        one.rejected_at = new Date();
      
      }else{
        return {
          status: false,
          message: 'Status is not valid',
          data: null,
        }
      }
      
      one.agenda = approvalDto.agenda ?? one.agenda;
      one.result = approvalDto.result ?? one.result;
      one.plan = approvalDto.plan ?? one.plan;
      
      await this.coachingMentoringRepository.save(one);
      
      return {
        status: true,
        message: 'success',
        data: one,
      }
      
    }catch(e){
      return {
        status: false,
        message: e.message,
        data: null,
      }
      // throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
    }
  }
}
