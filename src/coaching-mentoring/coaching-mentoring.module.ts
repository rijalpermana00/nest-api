import { Module } from '@nestjs/common';
import { CoachingMentoringService } from './coaching-mentoring.service';
import { CoachingMentoringController } from './coaching-mentoring.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CoachingMentoring } from './entities/coaching-mentoring.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CoachingMentoring])],
  controllers: [CoachingMentoringController],
  providers: [CoachingMentoringService]
})
export class CoachingMentoringModule {}
