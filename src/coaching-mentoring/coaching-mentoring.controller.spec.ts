import { Test, TestingModule } from '@nestjs/testing';
import { CoachingMentoringController } from './coaching-mentoring.controller';
import { CoachingMentoringService } from './coaching-mentoring.service';

describe('CoachingMentoringController', () => {
  let controller: CoachingMentoringController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CoachingMentoringController],
      providers: [CoachingMentoringService],
    }).compile();

    controller = module.get<CoachingMentoringController>(CoachingMentoringController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
