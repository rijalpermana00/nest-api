import { Transform } from 'class-transformer';
import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { v4 as uuid } from 'uuid';

@Entity({name : 'pfm_coaching_mentoring'})
export class CoachingMentoring{
    constructor(partial: Partial<CoachingMentoring>) {
        Object.assign(this, partial);
        if (!this.id) {
            this.id = uuid();
        }
    }
    
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    group_id: number;
    
    @Column()
    activity_id: number;

    @Column()
    agenda: string;
    
    @Column()
    leader_id: number;
    
    @Column()
    employee_id: number;
    
    @Column({length:250, nullable:true})
    result: string;
    
    @Column({length:250, nullable:true})
    plan: string;

    @Column({default:0})
    status: number;

    @Column({nullable:true})
    appointment_time: Date;

    @Column({nullable:true})
    approved_at: Date;

    @Column({nullable:true})
    rejected_at: Date;
    
    @CreateDateColumn({ type: 'timestamp' })
    @Transform(({ value }) => new Date(value))
    created_at: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    @Transform(({ value }) => new Date(value))
    updated_at: Date;
}