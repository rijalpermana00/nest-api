import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CoachingMentoringModule } from './coaching-mentoring/coaching-mentoring.module';
import { CoachingMentoring } from './coaching-mentoring/entities/coaching-mentoring.entity';

@Module({
  imports: [
    CoachingMentoringModule, 
    ConfigModule.forRoot({
      isGlobal:true,
      cache: true,
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DB_HOST,
      port: Number(process.env.DB_PORT),
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      entities: [CoachingMentoring],
      synchronize: true,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}