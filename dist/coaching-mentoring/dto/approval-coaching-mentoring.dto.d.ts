import { CreateCoachingMentoringDto } from './create-coaching-mentoring.dto';
declare const ApprovalCoachingMentoringDto_base: import("@nestjs/mapped-types").MappedType<Partial<CreateCoachingMentoringDto>>;
export declare class ApprovalCoachingMentoringDto extends ApprovalCoachingMentoringDto_base {
    status: string;
    plan: string;
    result: string;
}
export {};
