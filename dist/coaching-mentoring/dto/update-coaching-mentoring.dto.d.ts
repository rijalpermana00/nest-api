import { CreateCoachingMentoringDto } from './create-coaching-mentoring.dto';
declare const UpdateCoachingMentoringDto_base: import("@nestjs/mapped-types").MappedType<Partial<CreateCoachingMentoringDto>>;
export declare class UpdateCoachingMentoringDto extends UpdateCoachingMentoringDto_base {
    agenda: string;
    result: string;
    plan: string;
}
export {};
