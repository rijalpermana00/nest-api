export declare class CreateCoachingMentoringDto {
    groupId: number;
    activityId: number;
    agenda: string;
    leaderId: number;
    employeeId: number;
    appointmentTime: Date;
}
