"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApprovalCoachingMentoringDto = void 0;
const mapped_types_1 = require("@nestjs/mapped-types");
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const create_coaching_mentoring_dto_1 = require("./create-coaching-mentoring.dto");
class ApprovalCoachingMentoringDto extends (0, mapped_types_1.PartialType)(create_coaching_mentoring_dto_1.CreateCoachingMentoringDto) {
}
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_validator_1.IsEnum)({
        APPROVED: 'APPROVED',
        REJECTED: 'REJECTED'
    }),
    __metadata("design:type", String)
], ApprovalCoachingMentoringDto.prototype, "status", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], ApprovalCoachingMentoringDto.prototype, "plan", void 0);
__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsOptional)(),
    __metadata("design:type", String)
], ApprovalCoachingMentoringDto.prototype, "result", void 0);
exports.ApprovalCoachingMentoringDto = ApprovalCoachingMentoringDto;
//# sourceMappingURL=approval-coaching-mentoring.dto.js.map