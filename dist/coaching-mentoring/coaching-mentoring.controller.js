"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoachingMentoringController = void 0;
const common_1 = require("@nestjs/common");
const coaching_mentoring_service_1 = require("./coaching-mentoring.service");
const create_coaching_mentoring_dto_1 = require("./dto/create-coaching-mentoring.dto");
const update_coaching_mentoring_dto_1 = require("./dto/update-coaching-mentoring.dto");
const confirm_coaching_mentoring_dto_1 = require("./dto/confirm-coaching-mentoring.dto");
const approval_coaching_mentoring_dto_1 = require("./dto/approval-coaching-mentoring.dto");
let CoachingMentoringController = class CoachingMentoringController {
    constructor(coachingMentoringService) {
        this.coachingMentoringService = coachingMentoringService;
    }
    async create(createCoachingMentoringDto) {
        try {
            const coachingMentoring = await this.coachingMentoringService.create(createCoachingMentoringDto);
            return coachingMentoring;
        }
        catch (e) {
            throw new common_1.HttpException(e.message, common_1.HttpStatus.BAD_REQUEST);
        }
    }
    async confirm(id, confirmCoachingMentoringDto) {
        try {
            const coaching = await this.coachingMentoringService.confirm(id, confirmCoachingMentoringDto);
            return coaching;
        }
        catch (e) {
            throw new common_1.HttpException(e.message, common_1.HttpStatus.BAD_REQUEST);
        }
    }
    findAll() {
        return this.coachingMentoringService.findAll();
    }
    findOne(id) {
        return this.coachingMentoringService.findOne(id);
    }
    async update(id, updateCoachingMentoringDto) {
        try {
            const coaching = await this.coachingMentoringService.update(id, updateCoachingMentoringDto);
            return coaching;
        }
        catch (e) {
            throw new common_1.HttpException(e.message, common_1.HttpStatus.BAD_REQUEST);
        }
    }
    async approval(id, approvalDto) {
        try {
            const coaching = await this.coachingMentoringService.approval(id, approvalDto);
            return coaching;
        }
        catch (e) {
            throw new common_1.HttpException(e.message, common_1.HttpStatus.BAD_REQUEST);
        }
    }
};
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_coaching_mentoring_dto_1.CreateCoachingMentoringDto]),
    __metadata("design:returntype", Promise)
], CoachingMentoringController.prototype, "create", null);
__decorate([
    (0, common_1.Patch)('confirm/:id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, confirm_coaching_mentoring_dto_1.ConfirmCoachingMentoringDto]),
    __metadata("design:returntype", Promise)
], CoachingMentoringController.prototype, "confirm", null);
__decorate([
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], CoachingMentoringController.prototype, "findAll", null);
__decorate([
    (0, common_1.Get)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], CoachingMentoringController.prototype, "findOne", null);
__decorate([
    (0, common_1.Patch)('update/:id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_coaching_mentoring_dto_1.UpdateCoachingMentoringDto]),
    __metadata("design:returntype", Promise)
], CoachingMentoringController.prototype, "update", null);
__decorate([
    (0, common_1.Put)('approval/:id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, approval_coaching_mentoring_dto_1.ApprovalCoachingMentoringDto]),
    __metadata("design:returntype", Promise)
], CoachingMentoringController.prototype, "approval", null);
CoachingMentoringController = __decorate([
    (0, common_1.Controller)('coaching'),
    __metadata("design:paramtypes", [coaching_mentoring_service_1.CoachingMentoringService])
], CoachingMentoringController);
exports.CoachingMentoringController = CoachingMentoringController;
//# sourceMappingURL=coaching-mentoring.controller.js.map