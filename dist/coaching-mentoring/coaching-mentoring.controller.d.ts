import { CoachingMentoringService } from './coaching-mentoring.service';
import { CreateCoachingMentoringDto } from './dto/create-coaching-mentoring.dto';
import { UpdateCoachingMentoringDto } from './dto/update-coaching-mentoring.dto';
import { ConfirmCoachingMentoringDto } from './dto/confirm-coaching-mentoring.dto';
import { ApprovalCoachingMentoringDto } from './dto/approval-coaching-mentoring.dto';
export declare class CoachingMentoringController {
    private readonly coachingMentoringService;
    constructor(coachingMentoringService: CoachingMentoringService);
    create(createCoachingMentoringDto: CreateCoachingMentoringDto): Promise<import("./entities/coaching-mentoring.entity").CoachingMentoring>;
    confirm(id: string, confirmCoachingMentoringDto: ConfirmCoachingMentoringDto): Promise<import("./dto/response.dto").Response>;
    findAll(): Promise<import("./entities/coaching-mentoring.entity").CoachingMentoring[]>;
    findOne(id: string): Promise<import("./entities/coaching-mentoring.entity").CoachingMentoring>;
    update(id: string, updateCoachingMentoringDto: UpdateCoachingMentoringDto): Promise<{
        status: boolean;
        message: string;
        data: import("./entities/coaching-mentoring.entity").CoachingMentoring;
    } | {
        status: boolean;
        message: any;
        data: any;
    }>;
    approval(id: string, approvalDto: ApprovalCoachingMentoringDto): Promise<{
        status: boolean;
        message: string;
        data: import("./entities/coaching-mentoring.entity").CoachingMentoring;
    } | {
        status: boolean;
        message: any;
        data: any;
    }>;
}
