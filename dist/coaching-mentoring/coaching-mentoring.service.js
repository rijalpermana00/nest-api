"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoachingMentoringService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const coaching_mentoring_entity_1 = require("./entities/coaching-mentoring.entity");
let CoachingMentoringService = class CoachingMentoringService {
    constructor(coachingMentoringRepository) {
        this.coachingMentoringRepository = coachingMentoringRepository;
    }
    async create(createCoachingMentoringDto) {
        const coachingMentoring = new coaching_mentoring_entity_1.CoachingMentoring({
            group_id: createCoachingMentoringDto.groupId,
            activity_id: createCoachingMentoringDto.activityId,
            agenda: createCoachingMentoringDto.agenda,
            leader_id: createCoachingMentoringDto.leaderId,
            employee_id: createCoachingMentoringDto.employeeId,
            appointment_time: createCoachingMentoringDto.appointmentTime,
        });
        return await this.coachingMentoringRepository.save(coachingMentoring);
    }
    async findAll() {
        return await this.coachingMentoringRepository.find();
    }
    async findOne(id) {
        const one = await this.coachingMentoringRepository.findOne({
            where: { id: id },
        });
        return one;
    }
    async confirm(id, confirmCoachingMentoringDto) {
        try {
            const one = await this.findOne(id);
            console.log(one);
            if (!one) {
                throw new common_1.NotFoundException('Data not found');
            }
            one.agenda = confirmCoachingMentoringDto.agenda;
            one.appointment_time = confirmCoachingMentoringDto.appointmentTime;
            one.status = 1;
            const test = await this.coachingMentoringRepository.save(one);
            console.log(test);
            return {
                status: true,
                message: 'success',
                data: test,
            };
        }
        catch (e) {
            return {
                status: false,
                message: e.message,
                data: null,
            };
        }
    }
    async update(id, updateCoachingMentoringDto) {
        try {
            const one = await this.findOne(id);
            if (!one) {
                return {
                    status: false,
                    message: 'data not found',
                    data: null,
                };
            }
            if (one.status === 3) {
                return {
                    status: false,
                    message: 'data is already approved',
                    data: null,
                };
            }
            one.agenda = updateCoachingMentoringDto.agenda;
            one.result = updateCoachingMentoringDto.result;
            one.plan = updateCoachingMentoringDto.plan;
            one.status = 2;
            await this.coachingMentoringRepository.save(one);
            return {
                status: true,
                message: 'success',
                data: one,
            };
        }
        catch (e) {
            return {
                status: false,
                message: e.message,
                data: null,
            };
        }
    }
    async approval(id, approvalDto) {
        var _a, _b, _c;
        try {
            const one = await this.findOne(id);
            if (!one) {
                return {
                    status: false,
                    message: 'data not found',
                    data: null,
                };
            }
            if (one.status === 3) {
                return {
                    status: false,
                    message: 'data is already approved',
                    data: null,
                };
            }
            if (approvalDto.status === 'APPROVED') {
                one.status = 3;
                one.approved_at = new Date();
            }
            else if (approvalDto.status === 'REJECTED') {
                one.status = 4;
                one.rejected_at = new Date();
            }
            else {
                return {
                    status: false,
                    message: 'Status is not valid',
                    data: null,
                };
            }
            one.agenda = (_a = approvalDto.agenda) !== null && _a !== void 0 ? _a : one.agenda;
            one.result = (_b = approvalDto.result) !== null && _b !== void 0 ? _b : one.result;
            one.plan = (_c = approvalDto.plan) !== null && _c !== void 0 ? _c : one.plan;
            await this.coachingMentoringRepository.save(one);
            return {
                status: true,
                message: 'success',
                data: one,
            };
        }
        catch (e) {
            return {
                status: false,
                message: e.message,
                data: null,
            };
        }
    }
};
CoachingMentoringService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(coaching_mentoring_entity_1.CoachingMentoring)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], CoachingMentoringService);
exports.CoachingMentoringService = CoachingMentoringService;
//# sourceMappingURL=coaching-mentoring.service.js.map