import { Repository } from 'typeorm';
import { ApprovalCoachingMentoringDto } from './dto/approval-coaching-mentoring.dto';
import { ConfirmCoachingMentoringDto } from './dto/confirm-coaching-mentoring.dto';
import { CreateCoachingMentoringDto } from './dto/create-coaching-mentoring.dto';
import { Response } from './dto/response.dto';
import { UpdateCoachingMentoringDto } from './dto/update-coaching-mentoring.dto';
import { CoachingMentoring } from './entities/coaching-mentoring.entity';
export declare class CoachingMentoringService {
    private coachingMentoringRepository;
    constructor(coachingMentoringRepository: Repository<CoachingMentoring>);
    create(createCoachingMentoringDto: CreateCoachingMentoringDto): Promise<CoachingMentoring>;
    findAll(): Promise<CoachingMentoring[]>;
    findOne(id: string): Promise<CoachingMentoring>;
    confirm(id: string, confirmCoachingMentoringDto: ConfirmCoachingMentoringDto): Promise<Response>;
    update(id: string, updateCoachingMentoringDto: UpdateCoachingMentoringDto): Promise<{
        status: boolean;
        message: string;
        data: CoachingMentoring;
    } | {
        status: boolean;
        message: any;
        data: any;
    }>;
    approval(id: string, approvalDto: ApprovalCoachingMentoringDto): Promise<{
        status: boolean;
        message: string;
        data: CoachingMentoring;
    } | {
        status: boolean;
        message: any;
        data: any;
    }>;
}
