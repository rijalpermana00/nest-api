"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoachingMentoringModule = void 0;
const common_1 = require("@nestjs/common");
const coaching_mentoring_service_1 = require("./coaching-mentoring.service");
const coaching_mentoring_controller_1 = require("./coaching-mentoring.controller");
const typeorm_1 = require("@nestjs/typeorm");
const coaching_mentoring_entity_1 = require("./entities/coaching-mentoring.entity");
let CoachingMentoringModule = class CoachingMentoringModule {
};
CoachingMentoringModule = __decorate([
    (0, common_1.Module)({
        imports: [typeorm_1.TypeOrmModule.forFeature([coaching_mentoring_entity_1.CoachingMentoring])],
        controllers: [coaching_mentoring_controller_1.CoachingMentoringController],
        providers: [coaching_mentoring_service_1.CoachingMentoringService]
    })
], CoachingMentoringModule);
exports.CoachingMentoringModule = CoachingMentoringModule;
//# sourceMappingURL=coaching-mentoring.module.js.map