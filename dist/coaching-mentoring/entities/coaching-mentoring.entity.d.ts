export declare class CoachingMentoring {
    constructor(partial: Partial<CoachingMentoring>);
    id: string;
    group_id: number;
    activity_id: number;
    agenda: string;
    leader_id: number;
    employee_id: number;
    result: string;
    plan: string;
    status: number;
    appointment_time: Date;
    approved_at: Date;
    rejected_at: Date;
    created_at: Date;
    updated_at: Date;
}
